{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.ByteString (ByteString)
import qualified Data.Map.Strict as Map
import Test.HUnit

import Logstat.Types
import Logstat.Value
import Logstat.Parse
import Logstat.Eval


expr :: String -> ByteString -> Test
expr e res = e ~:
  case parseStmts (Config mempty mempty []) "-" ("set t " ++ e) of
    Left err -> assertFailure ("Parsing failed: " ++ err)
    Right stmts ->
      case snd $ step (head stmts) (head $ newState stmts) mempty of
        Left err' -> assertFailure ("Evaluation exception in " ++ show stmts ++ ":\n" ++ show err')
        Right v -> assertEqual (show stmts) res (asBS $ Map.findWithDefault undefined "t" v)


main :: IO Counts
main = runTestTT $ test
  [ expr "1+1" "2"
  , expr " 1 + 1 " "2"
  , expr "1.0 + 1" "2"
  , expr "0xff + 1" "256"
  , expr "0o777 + 1" "512"
  , expr "\"abc\"" "abc"
  , expr "\"abc\" . 1" "abc1"
  , expr "5 . 1" "51"
  , expr "(((1.1).1))" "1.11"
  , expr "2 + 3 * 5 - 1 / 2 + 5 % 3 " "18.5"
  , expr "2 +(3 * 5)-(1 / 2)+(5 % 3)" "18.5"
  , expr "11 == \"11\"" "1"
  , expr "1 == \"1.0000\"" "1"
  , expr "1 >= \"1.0001\"" ""
  , expr "1 <= \"1.0001\"" "1"
  , expr "!5" ""
  , expr "!!5" "1"
  , expr "!\"\"" "1"
  , expr "!\"a\" eq !1" "1"
  , expr "\"abc\" || 0" "abc"
  , expr "0 || \"abc\"" "abc"
  , expr "0 && \"abc\"" "0"
  , expr "1 && \"abc\"" "abc"
  , expr "2 ** 16" "65536"
  , expr "1 ? 2 : 3" "2"
  , expr "0 ? 1 : 2" "2"
  , expr "0 ? 1 : 2 ? 3 : 4" "3"
  , expr "1 + 2 * 3 == 7 ? (1?4:5) : 6" "4"
  , expr "1234 ~ /1/" "1"
  , expr "1234 !~ /5/" "1"
  , expr "\"abC\" ~ /^abc$/i" "1"
  , expr "\"abC\" !~ /^abc$/i" ""
  , expr "extract(123153, /1(.)3/)" "2"
  , expr "extract(123153, /1(.)3$/)" "5"
  , expr "replace(1, /1/, \"2\")" "2"
  , expr "replace(111, /1/, \"2\")" "222"
  , expr "replace(9181716, /1/, \"2\")" "9282726"
  , expr "replace(1, /3/, \"2\")" "1"
  ]
